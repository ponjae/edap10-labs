import lift.LiftView;

public class Main {

	public static void main(String[] args) {

		LiftView view = new LiftView();
		LiftMonitor monitor = new LiftMonitor();
		LiftThread l = new LiftThread(view, monitor);
		PersonThread[] xs = new PersonThread[20];

		l.start();
		for (int i = 0; i < 20; i++) {
			xs[i] = new PersonThread(view, monitor);
		}
		for (int i = 0; i < 20; i++) {
			xs[i].start();
		}

	}
}
