import lift.Passenger;

public class LiftMonitor {
	private int here = 0;
	private int next = 1;
	private int[] waitToEnterUp = new int[7];
	private int[] waitToEnterDown = new int[7];
	private int[] waitExit = new int[7];
	private int load = 0;
	private boolean liftIsMoving = false;
	private boolean isMovingUp = true;
	private int peopleInAction = 0;

	public synchronized void changeFloor() throws InterruptedException {
		int tmp;
		if (isMovingUp && here == 5) {
			tmp = here;
			here = next;
			next = tmp;
			isMovingUp = false;
		} else if (isMovingUp) {
			here = next;
			next = next + 1;
		} else if (here == 1) {
			tmp = here;
			here = next;
			next = tmp;
			isMovingUp = true;
		} else {
			here = next;
			next = next - 1;
		}
		liftIsMoving = false;
		notifyAll(); // vi kan ju inte notifiera de andra innan vi faktiskt har bytt våning.
	}

	public synchronized void holdLift() throws InterruptedException {
		while (((waitToEnterUp[here] > 0 && isMovingUp) && load < 4) || waitExit[here] > 0 || peopleInAction == 0
				|| ((waitToEnterDown[here] > 0 && !isMovingUp) && load < 4)) {
			notifyAll();
			wait();
		}
		liftIsMoving = true;
	}

	public synchronized int getHere() { // synchronized ??
		return here;
	}

	public synchronized int getNext() { // synchronized ??
		return next;
	}

	public synchronized void updateLists(int from, int to) {
		if (to - from > 0) {
			waitToEnterUp[from] = waitToEnterUp[from] + 1;
		} else {
			waitToEnterDown[from] = waitToEnterDown[from] + 1;
		}
		peopleInAction++;
		notifyAll();
	}

	public synchronized void waitToEnterLift(Passenger passenger) throws InterruptedException {
		int from = passenger.getStartFloor();
		int to = passenger.getDestinationFloor();
		int delta = to - from;
		while (from != here || liftIsMoving || load > 3 || (delta > 0) && !isMovingUp || (delta < 0) && isMovingUp) {
			wait();
		}
		load++;
		passenger.enterLift();
		if (delta > 0) {
			waitToEnterUp[passenger.getStartFloor()] = waitToEnterUp[passenger.getStartFloor()] - 1;
		} else {
			waitToEnterDown[passenger.getStartFloor()] = waitToEnterDown[passenger.getStartFloor()] - 1;
		}
		// waitToEnter[passenger.getStartFloor()] =
		// waitToEnter[passenger.getStartFloor()] - 1;
		waitExit[passenger.getDestinationFloor()] = waitExit[passenger.getDestinationFloor()] + 1;
		notifyAll();
	}

	public synchronized void waitToExitLift(Passenger passenger) throws InterruptedException {
		while (here != passenger.getDestinationFloor() || liftIsMoving) {
			wait();
		}
		peopleInAction--;
		load--;
		waitExit[passenger.getDestinationFloor()] = waitExit[passenger.getDestinationFloor()] - 1;
		passenger.exitLift();
//		peopleInAction--;
		notifyAll();
	}

}
