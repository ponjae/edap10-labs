import lift.LiftView;
import lift.Passenger;

public class PersonThread extends Thread {
	private LiftMonitor liftMonitor;
	private LiftView view;

	public PersonThread(LiftView view, LiftMonitor liftMonitor) {
		this.liftMonitor = liftMonitor;
		this.view = view;
	}

	@Override
	public void run() {
		while (true) {
			Passenger passenger = view.createPassenger();
			int from = passenger.getStartFloor();
			int to = passenger.getDestinationFloor();
			try {
				// waitToBegin(Math.round((Math.random() * 45000)));
				Thread.sleep(Math.round((Math.random()) * 45000));
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			passenger.begin();
			liftMonitor.updateLists(from, to);
			try {
				liftMonitor.waitToEnterLift(passenger);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				liftMonitor.waitToExitLift(passenger);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			passenger.end();
		}

		// private synchronized void waitToBegin(long millis) throws
		// InterruptedException {
		// long timeToWakeUp = System.currentTimeMillis() + millis;
		// long actualTime = timeToWakeUp - millis;
		// while (actualTime < timeToWakeUp) {
		// long dt = timeToWakeUp - actualTime;
		// wait(dt);
		// actualTime = System.currentTimeMillis();
		//
		// }
		// // labb2 notify(), slumpa tid, italiensk kö thread.sleep
		// }
	}
}
