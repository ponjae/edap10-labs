import lift.LiftView;

public class LiftThread extends Thread {
	private LiftView liftView;
	private LiftMonitor liftMonitor;

	public LiftThread(LiftView liftView, LiftMonitor liftMonitor) {
		this.liftView = liftView;
		this.liftMonitor = liftMonitor;
	}

	@Override
	public void run() {
		while (true) {
			try {
				liftMonitor.holdLift();
				liftView.moveLift(liftMonitor.getHere(), liftMonitor.getNext());
				liftMonitor.changeFloor();
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
	}

}
