
import java.math.BigInteger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import client.view.ProgressItem;
import client.view.StatusWindow;
import client.view.WorklistItem;
import network.Sniffer;
import network.SnifferCallback;
import rsa.Factorizer;
import rsa.ProgressTracker;

public class CodeBreaker implements SnifferCallback {

	private final JPanel workList;
	private final JPanel progressList;
	private final JProgressBar mainProgressBar;

	private ThreadFactory tf = Executors.defaultThreadFactory();
	private ExecutorService pool = Executors.newFixedThreadPool(2, tf);

	// -----------------------------------------------------------------------

	private CodeBreaker() {
		StatusWindow w = new StatusWindow();
		w.enableErrorChecks();
		workList = w.getWorkList();
		progressList = w.getProgressList();
		mainProgressBar = w.getProgressBar();

		new Sniffer(this).start();
	}

	// ------------------------------------------------------------------------

	public static void main(String[] args) throws Exception {

		/*
		 * Most Swing operations (such as creating view elements) must be performed in
		 * the Swing EDT (Event Dispatch Thread).
		 * 
		 * That's what SwingUtilities.invokeLater is for.
		 */

		SwingUtilities.invokeLater(() -> new CodeBreaker());
	}

	// ------------------------------------------------------------------------

	/** Called by a Sniffer thread when an encrypted message is obtained. */
	@Override
	public void onMessageIntercepted(String message, BigInteger n) {
		SwingUtilities.invokeLater(() -> {
			WorklistItem wli = new WorklistItem(n, message);
			ProgressItem pi = new ProgressItem(n, message);
			ProgressTracker tracker = new Tracker(pi, mainProgressBar);
			workList.add(wli);
			JButton button = new JButton("Break");
			JButton removeButton = new JButton("Remove");
			wli.add(button);
			removeButton.addActionListener(e -> {
					progressList.remove(pi);
					mainProgressBar.setValue(mainProgressBar.getValue() - 1000000);
					mainProgressBar.setMaximum(mainProgressBar.getMaximum() - 1000000);
			});
			button.addActionListener(e -> {
				workList.remove(wli);
				progressList.add(pi);
				mainProgressBar.setMaximum(mainProgressBar.getMaximum() + 1000000);
				pool.submit(() -> {
				//	System.out.println(Thread.currentThread().getName());
					String s = Factorizer.crack(message, n, tracker);
					SwingUtilities.invokeLater(() -> {
				//		System.out.println(Thread.currentThread().getName());
						pi.getTextArea().setText(s);
						pi.add(removeButton);
					});
				});
			});

		});
	}

	private static class Tracker implements ProgressTracker {
		private int totalProgress = 0;
		private int prevPercent = -1;
		private ProgressItem pi;
		private JProgressBar mainProgressBar;

		public Tracker(ProgressItem pi, JProgressBar mainProgressBar) {
			this.pi = pi;
			this.mainProgressBar = mainProgressBar;
		}

		@Override
		public void onProgress(int ppmDelta) {
			totalProgress += ppmDelta;
			int percent = totalProgress / 10000;
			SwingUtilities.invokeLater(() -> mainProgressBar.setValue(mainProgressBar.getValue() + ppmDelta));
			if (percent != prevPercent) {
				SwingUtilities.invokeLater(() -> {
					pi.getProgressBar().setString(Integer.toString(percent) + "%");
					pi.getProgressBar().setValue(totalProgress);
					prevPercent = percent;
				});
			}
		}
	}
}
