package factory.controller;

import factory.model.DigitalSignal;
import factory.model.WidgetKind;
import factory.swingview.Factory;

public class ToolController {
	private final DigitalSignal conveyor, press, paint;
	private final long pressingMillis, paintingMillis;
	private boolean isPressing = false;
	private boolean isPainting = false;

	public ToolController(DigitalSignal conveyor, DigitalSignal press, DigitalSignal paint, long pressingMillis,
			long paintingMillis) {
		this.conveyor = conveyor;
		this.press = press;
		this.paint = paint;
		this.pressingMillis = pressingMillis;
		this.paintingMillis = paintingMillis;
	}

	// alternativ ha tillstånd, 0, 1, 2 som markerar aktiva processer, om termen = 2
	// så sänk men sätt inte igång bandet...

	public synchronized void onPressSensorHigh(WidgetKind widgetKind) throws InterruptedException {
		//
		// TODO: you will need to modify this method
		//
		if (widgetKind == WidgetKind.BLUE_RECTANGULAR_WIDGET) {
			pressOn();
			// conveyor.off();
			// press.on();
			// waitOutside(pressingMillis);
			// press.off();
			// waitOutside(pressingMillis); // press needs this time to retract
			// conveyor.on();
		}
	}

	public synchronized void onPaintSensorHigh(WidgetKind widgetKind) throws InterruptedException {
		if (widgetKind == WidgetKind.ORANGE_ROUND_WIDGET) {
			paintOn();
			// conveyor.off();
			// paint.on();
			// waitOutside(paintingMillis);
			// paint.off();
			// conveyor.on();
		}
	}

	private void pressOn() throws InterruptedException {
		isPressing = true;
		conveyor.off();
		press.on();
		waitOutside(pressingMillis);
		pressOff();
	}

	private void paintOn() throws InterruptedException {
		isPainting = true;
		conveyor.off();
		paint.on();
		waitOutside(paintingMillis);
		paintOff();
	}

	private synchronized void pressOff() throws InterruptedException {
		press.off();
		waitOutside(pressingMillis);
		isPressing = false;
		notifyAll();
		while (isPainting) {
			wait();
		}
		conveyor.on();
	}

	private synchronized void paintOff() throws InterruptedException {
		paint.off();
		isPainting = false;
		notifyAll();
		while (isPressing) {
			wait();
		}
		conveyor.on();
	}

	private void waitOutside(long millis) throws InterruptedException {
		long timeToWakeUp = System.currentTimeMillis() + millis;
		long actualTime = timeToWakeUp - millis;
		while (actualTime < timeToWakeUp) {
			long dt = timeToWakeUp - actualTime;
			wait(dt);
			actualTime = System.currentTimeMillis();
		}
	}

	// -----------------------------------------------------------------------

	public static void main(String[] args) {
		Factory factory = new Factory();
		factory.startSimulation();
	}
}
