package lab;

import wash.WashingIO;

public class WaterController extends MessagingThread<WashingMessage> {

	private WashingIO io;
	private double prefLevel;
	private final int dt = 2;
	private boolean isActive = false;
	private boolean completeDrain = false;

	public WaterController(WashingIO io) {
		this.io = io;
	}

	@Override
	public void run() {

		try {
			MessagingThread<WashingMessage> mt = null;
			while (true) {
				WashingMessage m = receiveWithTimeout(dt / Wash.SPEEDUP);
				if (m != null) {
					mt = m.getSender();
					switch (m.getCommand()) {
					case (WashingMessage.WATER_FILL):
						prefLevel = m.getValue();
						isActive = true;
						break;
					case (WashingMessage.WATER_DRAIN):
						isActive = false;
						prefLevel = 0;
						completeDrain = true;
						break;
					case (WashingMessage.WATER_IDLE):
						isActive = false;
						io.drain(false);
						io.fill(false);
						break;
					}
				}

				if (isActive) {
					double delta = prefLevel - io.getWaterLevel();
					if (delta > 0.25) {
						io.fill(true);
						io.drain(false);
					} else if (delta < -0.41) {
						io.drain(true);
						io.fill(false);
					} else {
						isActive = false;
						io.drain(false);
						io.fill(false);
						mt.send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
					}
				}
				
				if(completeDrain) {
					io.drain(true);
					io.fill(false);
					if(io.getWaterLevel() == 0) {
						io.drain(false);
						completeDrain = false;
						mt.send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
					}
				}

			}
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}
}
