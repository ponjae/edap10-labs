package lab;

import wash.WashingIO;

public class SpinController extends MessagingThread<WashingMessage> {

	private WashingIO io;
	private boolean isSpinningSlow = false;
	private boolean isSpinningRight = true;

	public SpinController(WashingIO io) {
		this.io = io;
	}

	@Override
	public void run() {
		try {

			// ... TODO ...

			while (true) {
				// wait for up to a (simulated) minute for a WashingMessage
				WashingMessage m = receiveWithTimeout(60000 / Wash.SPEEDUP);

				// if m is null, it means a minute passed and no message was received
				if (m != null) {
					System.out.println("got " + m);
					switch (m.getCommand()) {

					case (WashingMessage.SPIN_SLOW):
						isSpinningSlow = true;
						break;
					case (WashingMessage.SPIN_FAST):
						io.setSpinMode(WashingIO.SPIN_FAST);
						break;

					case (WashingMessage.SPIN_OFF):
						io.setSpinMode(WashingIO.SPIN_IDLE);
						isSpinningRight = false;
						isSpinningSlow = false;
						break;

					}
				}
				if (isSpinningSlow) {
					if (isSpinningRight) {
						io.setSpinMode(WashingIO.SPIN_LEFT);
						isSpinningRight = false;
					} else {
						io.setSpinMode(WashingIO.SPIN_RIGHT);
						isSpinningRight = true;
					}
				}

				// ... TODO ...
			}
		} catch (InterruptedException unexpected) {
			// we don't expect this thread to be interrupted,
			// so throw an error if it happens anyway
			throw new Error(unexpected);
		}
	}
}
