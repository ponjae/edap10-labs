package lab;

import wash.WashingIO;

public class TemperatureController extends MessagingThread<WashingMessage> {

	private WashingIO io;
	private final double mu = 0.478;
	private final double ml = 0.00952;
	private int isHeating = 0;
	private double preftemp;
	private final int dt = 10;
	private boolean tempReach = true;
	private MessagingThread<WashingMessage> lastSender = null;

	public TemperatureController(WashingIO io) {
		this.io = io;
	}


	@Override
	public void run() {

		while (true) {

			try {
				WashingMessage m = receiveWithTimeout(dt / Wash.SPEEDUP);

				if (m != null) {
					lastSender = m.getSender();
					switch (m.getCommand()) {
					case (WashingMessage.TEMP_SET):
						tempReach = false;
						preftemp = m.getValue();
						if(io.getTemperature() < preftemp - mu - 0.2) {
							isHeating = 1;
						} else {
							isHeating = 0;
						}
						break;
					case (WashingMessage.TEMP_IDLE):
						preftemp = 0; // ev kolla
						break;
					}
				}

				handleHeating();
			
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
	}

	private void handleHeating() {
		switch (isHeating) {
		case 1:
			if (io.getTemperature() < preftemp - mu - 0.2) {
				io.heat(true);
			} else {
				io.heat(false);
				isHeating = 0;
			}
			break;

		case 0:
			if (io.getTemperature() < preftemp - 2 + ml + 0.2) {
				io.heat(true);
				isHeating = 1;
			} else {
				io.heat(false);
			}
			break;
		}

		double delta = preftemp - io.getTemperature();
		if (delta < 2 && !tempReach) {
			tempReach = true;
			lastSender.send(new WashingMessage(this, WashingMessage.ACKNOWLEDGMENT));
		}

	}
}
