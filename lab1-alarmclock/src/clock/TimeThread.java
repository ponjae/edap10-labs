package clock;

import java.util.concurrent.Semaphore;

public class TimeThread extends Thread{
	private SharedData sd;
	private ClockOutput co;
	private Semaphore mutex;

	public TimeThread(SharedData sd, ClockOutput co, Semaphore mutex) {
		this.sd = sd;
		this.co = co;
		this.mutex = mutex;
	}
	
	@Override
	public void run() {
			tickTime();
	}
	
	private void tickTime() {
		long t1;
		long diffTime;
		while(true) {
			t1 = System.currentTimeMillis();
			diffTime = (((t1 + 1000) / 1000) * 1000) - t1; 
			try {
				Thread.sleep(diffTime);
			} catch (Exception e) {
				throw new Error(e);
			}
			sd.tickTime();
			co.displayTime(sd.getDisplayingTime());
			mutex.release();						// ska signalera till alarm att en uppdatering skett
		}
	}
	
	
}

