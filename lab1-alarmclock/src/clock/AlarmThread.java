package clock;


import java.util.concurrent.Semaphore;

public class AlarmThread extends Thread {
	private SharedData sd;
	private Semaphore mutex;
	private int alarmCounter = 0;
	
	public AlarmThread(SharedData sd, Semaphore mutex) {
		this.sd = sd;
		this.mutex = mutex;
	}

	@Override
	public void run() {
		while(true) {
			if(alarmCounter > 0 && !sd.isButtonPressed()) {
				sd.displayAlarm();
				alarmCounter--;
			} else if (sd.checkAlarm()) {
					sd.displayAlarm();			// för att den inte ska bli fördröjd behövs alarmet displayas så fort checkAlarm blir sann.
					sd.beepAlarm();
					alarmCounter = 20;
			} else {
					alarmCounter = 0;
					sd.turnOffBeep();
			}
			try {	
				mutex.acquire();					//signalering
			} catch (InterruptedException e) {
				throw new Error(e);
			}
		}
	}
}
