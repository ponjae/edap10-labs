package clock;

import java.util.concurrent.Semaphore;

public class SharedData {  		//Är detta en monitor?
	private int time = 0;
	private int alarmTime = 0;			// måste börja från 0
	private boolean alarmStatus = false;
	private Semaphore mutex;
	private ClockOutput co;
	private boolean isButtonPressed = false;
	private boolean isBeeping = false;
	
	public SharedData(Semaphore timeMutex, ClockOutput co) {
		this.mutex = timeMutex;
		this.co = co;
	}
	
	public void updateAlarm(int newAlarmTime) throws InterruptedException { // Metod för att ändra alarmtiden. 
		mutex.acquire();
		alarmTime = setCurrentTimeInSec(newAlarmTime);
		isButtonPressed = false;
		mutex.release();
	}
	
	public void updateTime(int newTime) throws InterruptedException {		// Metod för att manuellt ändra tiden.
		mutex.acquire();
		time = setCurrentTimeInSec(newTime);
		isButtonPressed = false;
		mutex.release();
	}
	
	
	public void tickTime() {		// Metod för att öka tiden med en sekund varje gång anrop sker om inte tiden ska bli ett nytt dygn.
		try {
			mutex.acquire();
			if(time == 86399) {
				time = 0;
			} else {
			time += 1;
			}
			mutex.release();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
	}
	
	public int getTime() {			// behövs en acquire här, la till.
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		mutex.release();
		return time; 
	}
	
	public boolean checkAlarm(){		// Kollar om tiden är lika med alarmtiden samt att den är i "alarmläge".
		boolean ringAlarm = false;
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		ringAlarm = (alarmTime == time) && alarmStatus;
		mutex.release();
		return ringAlarm;
	}
	
	public int getDisplayingTime() {	// Metod för att få fram den int som visas på displayen.
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		int hours = (time / 3600) % 24;
		int mins = (time % 3600) / 60 ;
		int secs = time % 60;
		mutex.release();
		return (hours * 10000) + (mins * 100) + secs;
	}
	
	public boolean toggle() {	// Metod för att växla mellan alarmläge och neutral. Varför tmp?
		boolean tmp = false;
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		alarmStatus = ! alarmStatus;
		isButtonPressed = false;
		tmp = alarmStatus;
		mutex.release();
		return tmp;
	}

	private int setCurrentTimeInSec(int currentTime) {		// Metod för att omvandla en int i klockan till sekunder. Behövs ett lås?
		int hourSec = (currentTime / 10000) * 3600;
		int minSec = ((currentTime - ((currentTime / 10000) * 10000)) / 100) * 60;
		int secSec = currentTime %  100;
		return hourSec + minSec + secSec;
	}
	
	public void displayAlarm() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		co.alarm();
		mutex.release();
	}
	
	public void pressButton() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		if(isBeeping) {
		isButtonPressed = true;
		}
		mutex.release();
	}
	
	public boolean isButtonPressed() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		boolean tmpBool = isButtonPressed;
		mutex.release();
		return tmpBool;
	}
	
	public void beepAlarm() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		isBeeping = true;
		mutex.release();
		
	}
	
	public void turnOffBeep() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			throw new Error(e);
		}
		isBeeping = false;
		mutex.release();
	}
	
}

