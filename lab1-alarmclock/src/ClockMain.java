import java.util.concurrent.Semaphore;

import clock.AlarmThread;
import clock.ClockInput;
import clock.ClockInput.UserInput;
import clock.ClockOutput;
import clock.SharedData;
import clock.TimeThread;
import emulator.AlarmClockEmulator;

public class ClockMain {

    public static void main(String[] args) throws InterruptedException {
        AlarmClockEmulator emulator = new AlarmClockEmulator();

        ClockInput  in  = emulator.getInput();
        ClockOutput out = emulator.getOutput();

        Semaphore sem = in.getSemaphore();
        Semaphore mutex = new Semaphore(1);
        Semaphore signalMutex = new Semaphore(0);

        SharedData sd = new SharedData(mutex, out);
        TimeThread tt = new TimeThread(sd, out, signalMutex);
        AlarmThread at = new AlarmThread(sd,signalMutex);
        tt.start();
        at.start();
   
 
        while (true) {
        	sem.acquire();
        	UserInput userInput = in.getUserInput();
        	int choice = userInput.getChoice();
        	int value = userInput.getValue();
        	if(choice == 1) { 					// set time
        		sd.updateTime(value);
        		sd.pressButton();
        	} else if (choice == 2) { 			// set alarm
        		sd.updateAlarm(value);
        		sd.pressButton();
        	} else if (choice == 3 ) { 			// toggle alarm
        		out.setAlarmIndicator(sd.toggle());
        	} else {	
        		sd.pressButton();
        	}
        	
        }
    }
}


